variables:
  APP_VERSION: "${CI_PIPELINE_IID}"
  APP_NAME: "cars-api-demo"
  ARTIFACT_NAME: "cars-api-${CI_PIPELINE_IID}.jar"

stages:
  - build
  - test
  - deploy to staging
  - check staging
  - deploy to production

build:
  stage: build
  image: openjdk:12-alpine
  script:
    - env
    - sed -i "s/CI_PIPELINE_IID/$CI_PIPELINE_IID/" ./src/main/resources/application.yml
    - sed -i "s/CI_COMMIT_SHORT_SHA/$CI_COMMIT_SHORT_SHA/" ./src/main/resources/application.yml
    - sed -i "s/CI_COMMIT_BRANCH/$CI_COMMIT_BRANCH/" ./src/main/resources/application.yml
    - ./gradlew build
    - echo "ARTIFACT_NAME - ${ARTIFACT_NAME}"
    - mv ./build/libs/cars-api.jar ./build/libs/${ARTIFACT_NAME}
  artifacts:
    paths:
      - ./build/libs/

smoke test:
  stage: test
  when: manual
  image: openjdk:12-alpine
  before_script:
    - apk --no-cache add curl
  script:
    - java -jar ./build/libs/$ARTIFACT_NAME &
    - sleep 30
    - curl http://localhost:5000/actuator/health | grep "UP"

code quality:
  stage: test
  when: manual
  image: openjdk:12-alpine
  script:
    - ./gradlew pmdMain pmdTest
  artifacts:
    when: always
    paths:
      - build/reports/pmd

unit tests:
  stage: test
  when: manual
  image: openjdk:12-alpine
  script:
    - ./gradlew test
  artifacts:
    when: always
    paths:
      - build/reports/tests
    reports:
      junit: build/test-results/test/*.xml

deploy to staging:
  stage: deploy to staging
  when: manual
  environment:
    name: staging
    url: http://staging-cars-api-demo.us-east-1.elasticbeanstalk.com
  image:
    name: banst/awscli
    entrypoint: [""]
  before_script:
    - apk --no-cache add curl
    - apk --no-cache add jq
  script:
    - env
    - aws configure set region us-east-1
    - aws s3 cp ./build/libs/$ARTIFACT_NAME s3://$S3_BUCKET/$ARTIFACT_NAME
    - aws elasticbeanstalk create-application-version --application-name $APP_NAME --version-label $APP_VERSION --source-bundle S3Bucket=$S3_BUCKET,S3Key=$ARTIFACT_NAME
    - CNAME=$(aws elasticbeanstalk update-environment --application-name $APP_NAME --environment-name "staging" --version-label=$APP_VERSION | jq '.CNAME' --raw-output)
#    - echo "$CNAME"
#    - curl http://$CNAME/actuator/info | grep $CI_PIPELINE_IID
#    - curl http://$CNAME/actuator/health | grep "UP"

api testing:
  stage: check staging
  when: manual
  image:
    name: vdespa/newman
    entrypoint: [""]
  script:
    - newman --version
    - newman run "Cars-API.postman_collection.json" --environment Staging.postman_environment.json --reporters cli,htmlextra,junit --reporter-htmlextra-export "newman/report.html" --reporter-junit-export "newman/report.xml"
  artifacts:
    when: always
    paths:
      - newman/
    reports:
      junit: newman/report.xml

deploy to production:
  stage: deploy to production
  environment:
    name: production
    url: http://production-cars-api-demo.us-east-1.elasticbeanstalk.com
  when: manual
  image:
    name: banst/awscli
    entrypoint: [""]
  script:
    - aws configure set region us-east-1
    - aws elasticbeanstalk update-environment --application-name $APP_NAME --environment-name "production" --version-label=$APP_VERSION
